package ChatCLient;

import java.io.*;
import java.net.*;
import java.util.*;
import ChatLibrary.*;
// Client class
class ChatClient {
    private static String userName;
    public static void main(String[] args){
        try (Socket socket = new Socket("localhost", 420)) {
            System.out.println("connected");
            
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            ObjectOutputStream out  = new ObjectOutputStream(socket.getOutputStream());
            
            //dodac rejestracje
            
            userName="mariusz";
            Message message=new Message("LOGIN",userName,"SERVER",null);
            out.writeObject(message);
            System.out.println("Server replied :" + in.readLine());
            message.setType("DATA");
            out.reset();
            
            Scanner sc = new Scanner(System.in);
            String line = null;
            
            
            
            while (!"exit".equalsIgnoreCase(line)) {
                line = sc.nextLine();
                
                //dodac wybor odbiorcy
                message.setData(line);
                out.writeObject(message);
                out.reset();
                System.out.println("Server replied :" + in.readLine());
            }
            sc.close();
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
}